Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    get '/books',                                       to: 'books#fetch_books'
    get '/book/:id',                                    to: 'books#fetch_book'
    get '/book/:id/chapter/:chapter_id',                to: 'books#fetch_chapter'
    get '/book/:id/chapter/:book_id/verse/:verse_id',   to: 'books#fetch_verse'
    get '/book/:id/chapter/:book_id/passage/:start/:finish', to: 'books#fetch_passage'
  end
end
