module Api
  class BooksController < ApplicationController
    include Response
    include ExceptionHandler

    before_action :fetch_data
    before_action :set_book, :set_chapter, except: :fetch_books

    def fetch_books
      # result example
      # Results : {
      #   Book: Genesis,
      #   BookID: 0,
      #   TotalChapter: 10
      # }

      result = Hash.new
      book_list = Array.new

      book = @records['Book']

      for i in 0...book.length do
        temp_book = {
          Book: Book::BOOKS[i],
          BookID: i,
          TotalChapter: book[i]['Chapter'].length
        }

        book_list << temp_book
      end

      result[:Results] = book_list
      json_response result
    end

    def fetch_book
      # result example
      # Result: {
      #   Book: Genesis,
      #   Content: {
      #     Chapter: [{
      #         verseID: 0001,
      #         verse: some vers
      #       }]
      #   }
      # }

      result = {
        Result: {
          Book: Book::BOOKS[@book_id],
          Content: @book['Chapter']
        }
      }

      json_response result
    end

    def fetch_chapter
      # result example
      # Result: {
      #   Book: Genesis,
      #   ChapterID: 0,
      #   Content: [{
      #       verseID: 0001,
      #       Verse: something
      #     }]
      # }

      result = {
        Result: {
          Book: Book::BOOKS[@book_id],
          ChapterID: @chapter_id,
          Content: @chapter['Verse']
        }
      }

      json_response result
    end

    def fetch_verse
      # result example
      # Result: {
      #   Book: Genesis,
      #   ChapterID: 0,
      #   Verse: {
      #     verse from request id
      #   }
      # }

      verse_id = params[:verse_id]

      verse = Hash.new
      for i in 0...@chapter['Verse'].length do

        curr_verse = @chapter['Verse'][i]
        if curr_verse['Verseid'].eql?(verse_id)
          verse[:content], verse[:id] = curr_verse['Verse'], curr_verse['Verseid']
        end

      end

      raise ActiveRecord::RecordNotFound if verse[:content].nil?

      result = {
        Result: {
          Book: Book::BOOKS[@book_id],
          ChapterID: @chapter_id,
          VerseID: verse[:id],
          Passage: verse[:content]
        }
      }

      json_response result
    end

    def fetch_passage
      # result example
      # Result: {
      #   Book: Genesis,
      #   ChapterID: 0,
      #   Passage: {
      #     passage from start to finish
      #   }
      # }

      start = params[:start].to_i
      finish = params[:finish].to_i

      passage = ""
      for i in start..finish do
        passage += @chapter['Verse'][i]['Verse']
      end

      result = {
        Result: {
          Book: Book::BOOKS[@book_id],
          ChapterID: @chapter_id,
          Verse: "#{start} - #{finish}",
          Passage: passage
        }
      }

      json_response result
    end

    private
    def fetch_data
      path = File.join(Rails.root, 'vendor', 'godlytalias', 'json', 'bible.json')

      @records = JSON.parse(File.read(path))
    end

    def set_book
      @book_id = params[:id].to_i
      @book = @records['Book'][@book_id] || nil

      raise ActiveRecord::RecordNotFound if @book.nil?
    end

    def set_chapter
      @chapter_id = params[:chapter_id].to_i
      @chapter = @book['Chapter'][@chapter_id] || nil

      raise ActiveRecord::RecordNotFound if @chapter.nil?
    end
  end
end
