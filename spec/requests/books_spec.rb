require 'rails_helper'
require 'faker'

RSpec.describe 'Books API', type: :request do
  describe 'GET /api/books' do
    before { get '/api/books' }

    # it should respond with book names
    it 'returns books' do
      expect(json).not_to be_empty
    end

    it 'returns status code of 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /api/book/:id' do
    let(:book_id) { Book::BOOKS.index(Book::BOOKS.first) }
    before { get "/api/book/#{book_id}" }

    context 'when record exists' do
      it 'returns the book' do
        expect(json['Result']['Book']).to eq(Book::BOOKS[book_id])
        expect(json).not_to be_empty
      end

      it 'returns status code of 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when record does not exist' do
      let(:book_id) { Book::BOOKS.length }

      it 'returns status code of 404' do
        expect(response).to have_http_status(404)
      end
    end
  end

  before { import("godlytalias", "bible.json") } # prepare $records values from imported json file

  # prepare book_id and chapter_id values to be used for the rest of the tests
  let(:book_id) { Book::BOOKS.index(Book::BOOKS.first) }
  let(:chapter_id) { 0 } # since every sequence starts with 0

  describe 'GET /api/book/:id/chapter/:chapter' do
    before { get "/api/book/#{book_id}/chapter/#{chapter_id}" }

    context 'when record exists' do
      it 'returns the chapter content' do
        expect(json).not_to be_empty
        expect(json['Result']['ChapterID']).to be_equal(chapter_id)
      end

      it 'returns status code of 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when record does not exist' do
      let(:chapter_id) do
        $records['Book'].first['Chapter'].length
      end

      it 'returns status code of 404' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'GET /api/book/:id/chapter/:chapter/verse/:verse_id' do
    let(:verse_id) { $records['Book'].first['Chapter'].first['Verse'].first['Verseid'] }

    before { import("godlytalias", "bible.json") }
    before { get "/api/book/#{book_id}/chapter/#{chapter_id}/verse/#{verse_id}" }

    context 'when record exists' do
      it 'returns the chapter verses' do
        expect(json).not_to be_empty
        expect(json['Result']['VerseID']).to eq(verse_id)
      end

      it 'returns status code of 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when record does not exists' do
      let(:verse_id) { -1 }

      it 'returns status code of 404' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'GET /api/book/:id/chapter/:chapter/verse/:start/:finish' do
    let(:start) { 0 }
    let(:finish) { $records['Book'].first['Chapter'].first.length }

    before { import("godlytalias", "bible.json") }
    before { get "/api/book/#{book_id}/chapter/#{chapter_id}/passage/#{start}/#{finish}" }

    context 'when record exists' do
      it 'returns the requested passage' do
        expect(json).not_to be_empty
        expect(json['Result']['Verse']).to eq("#{start} - #{finish}")
      end

      it 'returns status code of 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
end
