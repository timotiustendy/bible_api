module ImportHelper
  def import(vendor, file_name)
    path = File.join(Rails.root, 'vendor', vendor, 'json', file_name)
    $records = JSON.parse(File.read(path))
  end
end
