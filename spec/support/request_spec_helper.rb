module RequestSpecHelper
  # helps to parse JSON response into ruby hash
  def json
    JSON.parse(response.body)
  end
end
